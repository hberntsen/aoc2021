module Main where

import qualified Input as I
import Test.HUnit
import Data.List(transpose,sort,group, sortOn)
import Data.Char (digitToInt)
import Data.Foldable (foldl')

-- https://stackoverflow.com/questions/5921573/convert-a-string-representing-a-binary-number-to-a-base-10-string-haskell
toDec :: String -> Int
toDec = foldl' (\acc x -> acc * 2 + digitToInt x) 0

gamma :: [String] -> String
gamma inp = map (head . (head . sortOn (negate . length) . (group . reverse. sort))) (transpose inp)
epsilon inp = map (head . (head . sortOn length . (group . sort))) (transpose inp)

bitCriteria :: ([String] -> String) -> [String] -> String
bitCriteria f [] = ""
bitCriteria f [i] = i
bitCriteria f is = let p = head (f is) in p : bitCriteria f (map tail $ filter (\x -> head x == p) is)

oxygen :: [String] -> String
oxygen = bitCriteria gamma

scrubber :: [String] -> String
scrubber = bitCriteria epsilon

lifeSupportRating is = toDec (oxygen is) * toDec (scrubber is)

tests = test [ "example gamma part 1" ~: 22 ~=? toDec (gamma I.ex)
             , "example epsilon part 1" ~: 9 ~=? toDec (epsilon I.ex)
             , "answer part 1" ~: 2972336 ~=? toDec( gamma I.puzzle) * toDec (epsilon I.puzzle)
             , "gamma chooses 1 on equal" ~: "1" ~=? gamma ["0", "1"]
             , "epsilon chooses 0 on equal" ~: "0" ~=? epsilon ["0", "1"]
             , "example oxygen part 2" ~: "10111" ~=? oxygen I.ex
             , "example scrubber part 2" ~: "01010" ~=? scrubber I.ex
             , "example lifeSupportRating" ~: 230 ~=? lifeSupportRating I.ex
             , "puzzle lifeSupportRating" ~: 3368358 ~=? lifeSupportRating I.puzzle
             ]

main = runTestTTAndExit tests
