{-# LANGUAGE ParallelListComp #-}
module Main where

import qualified Input as I
import qualified Data.Map as Map
import Data.Maybe (fromMaybe)
import Test.HUnit
import Lib (rangeFromTo)

lineCoords ((x1, y1), (x2, y2)) 
  | x1 == x2 || y1 == y2 = [ (x, y) | x <- rangeFromTo x1 x2, y <- rangeFromTo y1 y2]
  | otherwise            = [ (x, y) | x <- rangeFromTo x1 x2 | y <- rangeFromTo y1 y2]

part1 inp = part2 hv where
  hv = filter (\((x1, y1), (x2, y2)) -> x1 == x2 || y1 == y2) inp

part2 inp = length $ filter (>= 2) $ Map.elems map where
  map = foldr (Map.alter (Just . (+) 1 . fromMaybe 0)) Map.empty $ concatMap lineCoords inp

main = do
  Right iex <- I.parse "day5/input-ex.txt"
  Right ipu <- I.parse "day5/input.txt"
  runTestTTAndExit $ tests iex ipu

tests iex ipu =
  test [ "lineCoords1" ~: [(1,1), (1,2), (1,3)] ~=? lineCoords ((1,1),(1,3))
       , "lineCoords2" ~: [(9,7), (8,7), (7,7)] ~=? lineCoords ((9,7),(7,7))
       , "part1 example" ~: 5 ~=? part1 iex
       , "part1" ~: 8111 ~=? part1 ipu
       , "part2 example" ~: 12 ~=? part2 iex
       , "part2" ~: 22088 ~=? part2 ipu
       ]
