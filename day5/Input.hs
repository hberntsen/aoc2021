module Input(parse, Coord, Line) where

import Text.Parsec hiding (parse, Line)
import Text.Parsec.Char(char, newline)
import Text.Parsec.String (Parser, parseFromFile)
import Lib(int)

type Coord = (Integer, Integer)
type Line = (Coord, Coord)

line = do
  x1 <- int
  char ','
  y1 <- int
  string " -> "
  x2 <- int
  char ','
  y2 <- int
  newline
  return ((x1,y1),(x2,y2))

parser = many line
parse = parseFromFile parser
