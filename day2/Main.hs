module Main where

import qualified Input as I
import Test.HUnit

runCmd (h, d) (I.Forward x) = (h + x, d)
runCmd (h, d) (I.Down x) = (h, d + x)
runCmd (h, d) (I.Up x) = (h, d - x)

calcAnswer (h, d) = h * d
startState = (0, 0)
part1 = calcAnswer . foldl runCmd startState

runCmd2 (h, d, a) (I.Forward x) = (h + x, d + a * x, a)
runCmd2 (h, d, a) (I.Down x) = (h, d, a + x)
runCmd2 (h, d, a) (I.Up x) = (h, d, a - x)

startState2 = (0, 0, 0)
calcAnswer2 (h, d, a) = h * d
part2 = calcAnswer2 . foldl runCmd2 startState2

tests = test [ "example part1" ~: 150 ~=? part1 I.ex 
             , "puzzle part 1" ~: 1938402 ~=? part1 I.puzzle
             , "example part 2" ~: 900 ~=? part2 I.ex
             , "puzzle part 2" ~: 1947878632 ~=? part2 I.puzzle
             ]

main = runTestTTAndExit tests
