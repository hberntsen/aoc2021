{-# LANGUAGE ParallelListComp #-}
module Main where

import qualified Input as I

pairs l = [ (a, b) | a <- l | b <- drop 1 l]

-- 1462
increases l = length $ filter (uncurry (<)) $ pairs l
part1 = print $ increases $ pairs I.puzzle

triples l = [ [a, b, c] | a <- l | b <- drop 1 l | c <- drop 2 l]

-- 1497
part2 = increases $ map sum (triples I.puzzle)

main :: IO ()
main = print part2
