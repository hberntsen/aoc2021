module Input(parse) where

import Text.Parsec hiding (parse)
import Text.Parsec.Char(char, space, newline)
import Text.Parsec.String (Parser, parseFromFile)
import Data.Either (fromRight)
import Lib(int)

draws = int `sepBy` char ','

board = do
  c <- count 5 $ do
    many (char ' ')
    nums <- int `sepBy` many1 (char ' ')
    newline
    return nums
  newline
  return c

parser = do
  d <- draws
  newline
  newline
  b <- many board
  eof
  return (d,b)

parse = parseFromFile parser

--main = puzzleInput >>= print
main = parseFromFile parser "input.txt" >>= print
--main = print 1


