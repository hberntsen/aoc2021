{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import qualified Input as I
import Data.List (transpose, inits, find)
import Data.Maybe (fromJust)

unmarked d = filter (`notElem` d) . concat
checkBoard d b = any (checkRow d) b || any (checkRow d) (transpose b)
checkRow d = all (`elem` d)

part1 = do 
  Right (draws, boards) <- I.parse "day4/input.txt"
  let (wd, wbs) = head $ filter (not . null . snd) $ map (\d -> (d, filter (checkBoard d) boards)) (inits draws)
  let wb = head wbs
  let sun = sum $ unmarked wd wb
  print wd
  print wb
  print "Sum of unmarked numbers" 
  print (sun * last wd)

part2 = do
  Right (draws, boards) <- I.parse "day4/input.txt"
  let winning = filter (checkBoard draws) boards
  let wbs1 = fromJust $ find (\bs -> length bs == length winning - 1) $ map (\d -> filter (checkBoard d) boards) (inits draws)
  let lwb :: [[Integer]] = head $ filter (`notElem` wbs1) winning
  let lwd = fromJust $ find (\dws -> checkBoard dws lwb) (inits draws)
  let sun = sum $ unmarked lwd lwb
  print lwd
  print lwb
  print "Sum of unmarked numbers" 
  print (sun * last lwd)



main = part2
