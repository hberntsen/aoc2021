module Lib
    ( int
    , rangeFromTo
    ) where

import Text.Parsec.String (Parser)
import Text.Parsec (many1)
import Text.Parsec.Char (digit)

--https://stackoverflow.com/a/24171275
int :: Parser Integer
int = read <$> many1 digit

rangeFromTo a b
  | a > b = reverse [b..a]
  | otherwise = [a..b]
